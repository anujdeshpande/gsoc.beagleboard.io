.. _proposal_template:

Proposal template 
#################

Introduction
*************

Summary links
=============

- **Student:** ``@ayush1325``
- **Mentors:** ``@jkridner``, ``@vaishnav``
- **Code:** `Google Summer of Code / greybus / cc1352-firmware · GitLab <https://openbeagle.org/gsoc/greybus/cc1352-firmware>`_
- **Documentation:** `Ayush Singh / docs.beagleboard.io · GitLab <https://openbeagle.org/ayush1325/docs.beagleboard.io>`_
- **GSoC:** `Google Summer of Code <https://summerofcode.withgoogle.com/archive/2023/projects/iTfGBkDk>`_ 

Status
=======

This project is currently just a proposal.

Proposal
========

Please complete the requirements listed on the `GSoc Ideas <https://forum.beagleboard.org/t/gsoc-ideas/35850/1>`_ 
and fill out this template.

About 
=====

- **Forum:** ``@ayush1325``
- **OpenBeagle:** `Ayush Singh · GitLab <https://openbeagle.org/ayush1325>`_
- **IRC:**
- **Github:** `jadonk (Jason Kridner) · GitHub <https://github.com/jadonk>`_
- **School:** Greatest University
- **Country:** Worldistan
- **Primary language:** Igpay Atinlay
- **Typical work hours:** 8AM-5PM US Eastern
- **Previous GSoC participation:** 

Project
********

**Project name:** About my super cool project.

Description
============

In 10-20 sentences, what are you making, for whom, why and with what technologies 
(programming languages, etc.)? (We are looking for open source SOFTWARE submissions.)

Software
=========

Which software or technology stack are you going to use to complete this project.

Hardware
========

A list of hardware that you are going to use for this project.

Timeline
********

Provide a development timeline with 10 milestones, one for each week of development without 
an evaluation, and any pre-work. (A realistic, measurable timeline is critical to our selection process.)

.. note:: This timeline is based on the `official GSoC timeline <https://developers.google.com/open-source/gsoc/timeline>`_


Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | February 26            | Connect with possible mentors and request review on first draft                                    |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 4                | Complete prerequisites, verify value to community and request review on second draft               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 11               | Finalized timeline and request review on final draft                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 21               | Submit application                                                                                 |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | Start bonding                                                                                      |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 27                 | Start coding and introductory video                                                                |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | Release introductory video and complete milestone #1                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | Complete milestone #2                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | Complete milestone #3                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | Complete milestone #4                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | Complete milestone #5                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | Submit midterm evaluations                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | Complete milestone #6                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | Complete milestone #7                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | Complete milestone #8                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | Complete milestone #9                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | Complete milestone #10                                                                             |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19              | Submit final project video, submit final work to GSoC site and complete final mentor evaluation    |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

Community Bonding Period (May 1st - May 26th)
==============================================

GSoC contributors get to know mentors, read documentation, get up to speed to begin working on their projects

Coding begins (May 27th)
=========================

Milestone #1, Introductory YouTube video (June 3rd)
===================================================

Milestone #2 (June 10th)
==========================

Milestone #3 (June 17th)
=========================

Milestone #4 (June 24th)
==========================

Milestone #5 (July 1st)
========================

Submit midterm evaluations (July 8th)
=====================================

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

Milestone #6 (July 15th)
=========================

Milestone #7 (July 22nd)
=========================

Milestone #8 (July 29th)
=========================

Milestone #9 (Aug 5th)
=======================

Milestone #10 (Aug 12th)
========================

Final YouTube video (Aug 19th)
===============================

Submit final project video, submit final work to GSoC site 
and complete final mentor evaluation

Final Submission (Aug 24nd)
============================

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
=============================

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with extended deadline

Experience and approch
***********************

In 5-15 sentences, convince us you will be able to successfully complete your project in the timeline you have described.

Contingency
===========

What will you do if you get stuck on your project and your mentor isn’t around?

Benefit
========

If successfully completed, what will its impact be on the `BeagleBoard.org <http://beagleboard.org/>`_ community? Include quotes from `BeagleBoard.org <http://beagleboard.org/>` 
community members who can be found on `Discord <https://discord.com/invite/e58xECGWfR>`_ and `BeagleBoard.org forum <https://forum.beagleboard.org/c/gsoc/13>`_.

Misc
====

Please complete the requirements listed in the `General Requirements <https://forum.beagleboard.org/t/gsoc-ideas/35850#general-requirements-5>`_. 
Provide link to merge request.

Suggestions
===========

Is there anything else we should have asked you?
.. gsoc.beagleboard.io documentation master file, created by
   sphinx-quickstart on Fri Jan 19 02:18:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:html_theme.sidebar_secondary.remove: true

GSoC @ BeagleBoard.org
#######################

Work on awesome open source projects this summer with BeagleBoard.org!

.. admonition:: What is Google Summer of Code?
   :class: admonition-code

   Spend your summer break writing code and learning about open source development while earning money!
   Accepted students work with a mentor and become a part of the open source community. Many become lifetime
   open source developers! The 2024 student application window will be open from
   `March 18th 2024 <https://developers.google.com/open-source/gsoc/timeline#march_18_-_1800_utc>`_ to
   `April 2nd 2024 <https://developers.google.com/open-source/gsoc/timeline#april_2_-_1800_utc>`_!
   But don't wait for then to engage! Come to our `Discord <https://discord.com/invite/e58xECGWfR>`_ and
   `Forum <https://forum.beagleboard.org/c/gsoc>`_ to share ideas today.

.. note::

   Google Summer of Code is open to individuals age 18 and older in most countries who are new or beginner
   contributors to open source coding projects. Read more on the GSoC site
   `Rules page <https://summerofcode.withgoogle.com/rules>`_ and the
   `FAQ page <https://developers.google.com/open-source/gsoc/faq>`_.

.. grid:: 1 2 2 3

   .. grid-item-card::
      :link: https://openbeagle.org/

      :fab:`gitlab;pst-color-primary` Open Source Development
      ^^^^^^^^^^

      Experience working on impactful open source project which
      will be used by thousands of people around the world!

   .. grid-item-card::
      :link: https://www.beagleboard.org/boards

      :fas:`microchip;pst-color-primary` Free Hardware
      ^^^^^^^^^^

      Recieve BeagleBoard.org open source single board computer,
      debugging tools, and other required hardware for free!

   .. grid-item-card::
      :link: https://developers.google.com/open-source/gsoc/help/student-stipends

      :fas:`sack-dollar;pst-color-primary` Handsome Stipend
      ^^^^^^^^^^

      Earn while making impactful contributions to the open source
      community, Stipend can be upto $6600 based on your country.

.. admonition:: BeagleBoard.org background
   :class: admonition-clock-back

   BeagleBoard.org is a volunteer organization that seeks to advance the state of
   `open-source software <http://en.wikipedia.org/wiki/Open-source_software>`_ on `open-source
   hardware <https://en.wikipedia.org/wiki/Open-source_hardware>`_ platforms capable of
   running high-level languages and operating systems (like Linux and Zephyr) in embedded
   environments. Born from taking mobile phone processors and putting them on low-cost boards to build affordable
   desktop computers, BeagleBoard.org has evolved to focus on the needs of the "maker" community with greater focus
   on the I/O needed for controlling motors and reading sensors to build things like robots, 3d printers, flying
   drones, in-car computer systems and much more.  BeagleBoard has `inspired creation of the Raspberry Pi <https://web.archive.org/web/20120302232755/www.linuxuser.co.uk/features/raspberry-pi-interview-eban-upton-reveals-all>`_, built over 6 million computers and made PocketBeagle available world-wide for about $25, but is more than a throw-away computer. It is an instance
   of true open hardware, exposing users to the broader world of electronics, demystifying computers and fostering an environment
   of clones that have changed the industry for good.

   Learn more at https://www.beagleboard.org/about.

.. admonition:: Some inspirational past projects
   :class: admonition-youtube hint

   Get inspiration from some :ref:`Past_Projects`.

.. grid:: 1 1 3 3

   .. grid-item-card::

      .. youtube:: -giV6Xr8RtY
         :width: 100%

   .. grid-item-card::

      .. youtube:: RWBzyHNetOE
         :width: 100%

   .. grid-item-card::

      .. youtube:: CDbEAq33vdA
         :width: 100%

.. important::

   Students will be expected to execute a series of prerequisites to demonstrate and
   expand familiarity with embedded systems development. Don't worry, the
   `live chat <https://bbb.io/gsocchat>`_ channel has over 1,000 active members to
   travel with you on your journey.


.. admonition:: Some other amazing places to find Beagles
   :class: hint

   Beagles have been to the depths of space and the oceans, as well as
   made cheeseburgers. Check the below projects made by incredible
   professional innovators for some inspiration.

.. grid:: 1 2 2 2

   .. grid-item-card::

      :fas:`rocket;pst-color-primary` Europa Rover Prototype
      ^^^^^^^^^^

      .. youtube:: sY5WQG3-3mo
         :width: 100%

   .. grid-item-card::

      :fas:`burger;pst-color-primary` Cheeseburger Robot
      ^^^^^^^^^^

      .. youtube:: CbL_3le40qc
         :width: 100%

.. admonition:: Explore some specific project ideas for this year!
   :class: hint

   Now you are ready to explore this year's :ref:`gsoc-project-ideas`.

.. toctree::
   :maxdepth: 2
   :hidden:

   ideas/index
   proposals/index
   contrib/index
   projects/index

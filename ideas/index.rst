.. _gsoc-project-ideas:

Ideas
######

Below are the project ideas for the GSoC 2024 sorted for better navigation. The BeagleBoard.org community is loaded 
with experts on building open source embedded systems. An embedded system is build of computers 
dedicated a specific collections of functions and connected to automate things in the physical world.

.. important:: 
    BeagleaBoard.org is a community centric organization and we keep all of our discussion open for our 
    community on `forum <https://forum.beagleboard.org/>`_. Discussion for all the ideas/projects will 
    be done via `forum <https://forum.beagleboard.org/>`_ as well and must be open for everyone to access. **No 
    direct messages should be sent to mentors or community members until unless there is a private matter.**

All the projects have colorful badges for making your choice easier,

.. table:: 

    +------------------------------------+------------------------------------+------------------------------------+
    | Priority                           | Complexity                         | Size                               |
    +====================================+====================================+====================================+
    | :bdg-danger:`High priority`        | :bdg-danger:`High complexity`      | :bdg-danger-line:`Large size`      |
    +------------------------------------+------------------------------------+------------------------------------+   
    | :bdg-success:`Medium priority`     | :bdg-success:`Medium complexity`   | :bdg-success-line:`Medium size`    |
    +------------------------------------+------------------------------------+------------------------------------+ 
    | :bdg-info:`Low priority`           | :bdg-info:`Low complexity`         | :bdg-info-line:`Small size`        |
    +------------------------------------+------------------------------------+------------------------------------+  

.. admonition:: Did you know?

    BeagleBoard.org has been accepted to be a mentoring organization in the Google Summer of Code (GSoC) for twelve previous years!

.. note::
    
    We don't want to just make more things, we want to enabled individuals to make the things that dominate their lives, 
    rather than leaving it up to someone else. Google Summer of a Code with BeagleBoard.org is a great way to learn 
    skills highly in demand while making a difference in the world. 

.. toctree:: 
    :maxdepth: 2
    :hidden:
    :caption: Project Ideas

    linux-kernel-improvements 
    rtos-microkernel-improvements
    deep-learning 
    beagleconnect-technology 
    automation-and-industrial-io 
    soft-peripherals-using-co-processors 
    security-and-privacy
    beaglebone-audio-applications
    bela-applications
    fpga-projects
    other-projects



